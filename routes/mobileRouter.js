const express = require("express");

var db = require("../controllers/databaseController");

var userMiddleware = require("../middleware/userMiddleware.js");

var router = express.Router();

const passwordForCrypt = "9sd921iufds8";

router.get("/", (req, res) => { res.send("OK"); })

// Login 

router.post("/login", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/login z ip naslova: " + ip);

    var email = req.body.email;
    var password = req.body.password;
    db.query("SELECT user_id, sec_token FROM users WHERE email = ? AND password = ?;", [email, password], (err, dataOfUser) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var backData = {
                user_id : -1,
                sec_token : -1
            };

            if (dataOfUser[0] != null) {
                backData.user_id = dataOfUser[0].user_id;
                backData.sec_token = dataOfUser[0].sec_token;
            }

            res.send(backData);
        }
    });
});

//Pridobivanje podatkov

router.get("/getUserAndBandData", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getUserData z ip naslova: " + ip);

    var userData = {
        user_id : req.body.user_id,
        user_name : new String(),
        user_surname : new String(),
        band_id : new Number(),
        band_name : new String(),
        band_description : new String(),
        band_address : new String(),
        band_type : new String()
    };

    db.query("SELECT users.user_id, users.name as user_name, users.surename as user_surname, bands.band_id, bands.band_name, bands.description as band_description, addresses.address as band_address, citys.name as city_name, countrys.name as country_name, types_of_bands.type as band_type FROM users JOIN bands ON (users.band_id = bands.band_id) JOIN addresses ON (addresses.address_id = bands.address_id) JOIN types_of_bands ON (types_of_bands.type_of_band_id = bands.type_of_band_id) JOIN citys ON (citys.city_id = addresses.city_id) JOIN countrys ON (countrys.country_id = addresses.country_id) WHERE users.user_id = ? LIMIT 1;", req.body.user_id, (err, userDataDB) => {
        if (err) res.send("Prislo je do napake: " + err);
        else {
            userData.user_name = userDataDB[0].user_name;
            userData.user_surname = userDataDB[0].user_surname;
            userData.band_id = userDataDB[0].band_id;
            userData.band_name = userDataDB[0].band_name;
            userData.band_description = userDataDB[0].band_description;
            userData.band_address = userDataDB[0].band_address + ", " + userDataDB[0].city_name + ", " + userDataDB[0].country_name;
            userData.band_type = userDataDB[0].band_type;

            res.send(userData);
        }
    });
});

router.get("/getBandSongsID/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandSongs/:band_id z ip naslova: " + ip);

    var band_id = req.params.band_id;

    db.query("SELECT band_song_id FROM band_has_songs WHERE band_id = ?;", band_id, (err, bandSongsIDS) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongsIDS);
        }
    });
});

router.get("/getSongsInfo/:band_song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile//getSongsInfo/:band_song_id z ip naslova: " + ip);

    var bandSongID = req.params.band_song_id;

    db.query("SELECT songs.song_id, songs.title, songs.lyrics, songs.order_of_parts, band_songs.order_number, band_songs.tunning FROM band_songs JOIN songs ON band_songs.song_id = songs.song_id WHERE band_songs.band_song_id = ?;", bandSongID, (err, bandSongInfo) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongInfo[0]);
        }
    });
});

router.get("/getSongTypeID/:song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongTypeID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT type_of_song_id FROM types_of_songs_has_songs WHERE song_id = ?", songID, (err, typesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typesIDs);
        }
    });
});

router.get("/getSongType/:type_of_song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongType/:type_of_song_id z ip naslova: " + ip);

    var typeOfSongID = req.params.type_of_song_id;

    db.query("SELECT type FROM types_of_songs WHERE type_of_song_id = ?", typeOfSongID, (err, typeOfSong) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typeOfSong[0]);
        }
    });
});

router.get("/getSongArtistID/:song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongArtistID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT artist_id FROM artists_has_songs WHERE song_id = ?", songID, (err, artistIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistIDs);
        }
    });
});

router.get("/getArtistData/:artist_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getArtistData/:artist_id z ip naslova: " + ip);

    var artistID = req.params.artist_id;

    db.query("SELECT name FROM artists WHERE artist_id = ?", artistID, (err, artistData) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistData[0]);
        }
    });
});

router.get("/getBandPerfomances/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandPerfomances/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT performance_id, date, time, description, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM perfomances JOIN addresses ON (perfomances.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE perfomances.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.get("/getBandPractices/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandPractices/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT practice_id, date, time, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM practices JOIN addresses ON (practices.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE practices.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.get("/getPracticeSongsID/:practice_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getPracticeSongsID/:practice_id z ip naslova: " + ip);

    var practiceID = req.params.practice_id;

    db.query("SELECT band_song_id FROM practices_songs WHERE practice_id = ?;", practiceID, (err, practicesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(practicesIDs);
        }
    });
});

////////////////////////////////////////////////////////
// ZA HERCOGA
////////////////////////////////////////////////////////

// Login 

router.post("/login/:email/:password", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/login z ip naslova: " + ip);

    var email = req.params.email;
    var password = req.params.password;
    db.query("SELECT user_id, sec_token FROM users WHERE email = ? AND password = ?;", [email, password], (err, dataOfUser) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var backData = {
                user_id : -1,
                sec_token : -1
            };

            if (dataOfUser[0] != null) {
                backData.user_id = dataOfUser[0].user_id;
                backData.sec_token = dataOfUser[0].sec_token;
            }

            res.send(backData);
        }
    });
});

//Pridobivanje podatkov

router.get("/getUserAndBandData/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getUserData z ip naslova: " + ip);

    var userData = {
        user_id : req.params.user_id,
        user_name : new String(),
        user_surname : new String(),
        band_id : new Number(),
        band_name : new String(),
        band_description : new String(),
        band_address : new String(),
        band_type : new String()
    };

    db.query("SELECT users.user_id, users.name as user_name, users.surename as user_surname, bands.band_id, bands.band_name, bands.description as band_description, addresses.address as band_address, citys.name as city_name, countrys.name as country_name, types_of_bands.type as band_type FROM users JOIN bands ON (users.band_id = bands.band_id) JOIN addresses ON (addresses.address_id = bands.address_id) JOIN types_of_bands ON (types_of_bands.type_of_band_id = bands.type_of_band_id) JOIN citys ON (citys.city_id = addresses.city_id) JOIN countrys ON (countrys.country_id = addresses.country_id) WHERE users.user_id = ? LIMIT 1;", req.body.user_id, (err, userDataDB) => {
        if (err) res.send("Prislo je do napake: " + err);
        else {
            userData.user_name = userDataDB[0].user_name;
            userData.user_surname = userDataDB[0].user_surname;
            userData.band_id = userDataDB[0].band_id;
            userData.band_name = userDataDB[0].band_name;
            userData.band_description = userDataDB[0].band_description;
            userData.band_address = userDataDB[0].band_address + ", " + userDataDB[0].city_name + ", " + userDataDB[0].country_name;
            userData.band_type = userDataDB[0].band_type;

            res.send(userData);
        }
    });
});

router.get("/getBandSongsID/:band_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandSongs/:band_id z ip naslova: " + ip);

    var band_id = req.params.band_id;

    db.query("SELECT band_song_id FROM band_has_songs WHERE band_id = ?;", band_id, (err, bandSongsIDS) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongsIDS);
        }
    });
});

router.get("/getSongsInfo/:band_song_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile//getSongsInfo/:band_song_id z ip naslova: " + ip);

    var bandSongID = req.params.band_song_id;

    db.query("SELECT songs.song_id, songs.title, songs.lyrics, songs.order_of_parts, band_songs.order_number, band_songs.tunning FROM band_songs JOIN songs ON band_songs.song_id = songs.song_id WHERE band_songs.band_song_id = ?;", bandSongID, (err, bandSongInfo) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongInfo[0]);
        }
    });
});

router.get("/getSongTypeID/:song_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongTypeID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT type_of_song_id FROM types_of_songs_has_songs WHERE song_id = ?", songID, (err, typesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typesIDs);
        }
    });
});

router.get("/getSongType/:type_of_song_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongType/:type_of_song_id z ip naslova: " + ip);

    var typeOfSongID = req.params.type_of_song_id;

    db.query("SELECT type FROM types_of_songs WHERE type_of_song_id = ?", typeOfSongID, (err, typeOfSong) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typeOfSong[0]);
        }
    });
});

router.get("/getSongArtistID/:song_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getSongArtistID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT artist_id FROM artists_has_songs WHERE song_id = ?", songID, (err, artistIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistIDs);
        }
    });
});

router.get("/getArtistData/:artist_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getArtistData/:artist_id z ip naslova: " + ip);

    var artistID = req.params.artist_id;

    db.query("SELECT name FROM artists WHERE artist_id = ?", artistID, (err, artistData) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistData[0]);
        }
    });
});

router.get("/getBandPerfomances/:band_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandPerfomances/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT performance_id, date, time, description, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM perfomances JOIN addresses ON (perfomances.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE perfomances.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.get("/getBandPractices/:band_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getBandPractices/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT practice_id, date, time, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM practices JOIN addresses ON (practices.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE practices.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.get("/getPracticeSongsID/:practice_id/:user_id/:sec_token", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /mobile/getPracticeSongsID/:practice_id z ip naslova: " + ip);

    var practiceID = req.params.practice_id;

    db.query("SELECT band_song_id FROM practices_songs WHERE practice_id = ?;", practiceID, (err, practicesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(practicesIDs);
        }
    });
});

module.exports = router;

const express = require("express");

var db = require("../controllers/databaseController.js");

var userMiddleware = require("../middleware/userMiddleware.js");

var router = express.Router();

router.get("/", (req, res) => { res.send("OK"); })

router.get("/test", (req, res) => {
    var testArray = req.body.polje;

    console.log(testArray);

    res.send(testArray[1]);
});

// Registracija in login

router.post("/register", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/register z ip naslova: " + ip);
    
    var name = req.body.name;
    var surname = req.body.surname;
    var email = req.body.email;
    var password = req.body.password;
    var secToken = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    db.query("SELECT EXISTS(SELECT * FROM users WHERE email = ?) AS obstaja;", email, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO users VALUES (null, 1, ?, ?, ?, ?, 0, ?);", [name, surname, email, password, secToken], (err, newUserID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Napak pri vstavlanju: " + err);
                    } else {
                        var backData = {
                            user_id : newUserID.insertId,
                            sec_token : secToken
                        };
                        res.send(backData);
                    }
                });
            } else {
                res.send( { user_id : -1, sec_token : -1 } );
            }
        }
    });
});

router.post("/login", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/login z ip naslova: " + ip);

    var email = req.body.email;
    var password = req.body.password;
    db.query("SELECT user_id, sec_token FROM users WHERE email = ? AND password = ?;", [email, password], (err, dataOfUser) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var backData = {
                user_id : -1,
                sec_token : -1
            };

            if (dataOfUser[0] != null) {
                backData.user_id = dataOfUser[0].user_id;
                backData.sec_token = dataOfUser[0].sec_token;
            }

            res.send(backData);
        }
    });
});

//Pridobivanje podatkov

router.post("/getUserAndBandData", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getUserData z ip naslova: " + ip);

    var userData = {
        user_id : req.body.user_id,
        user_name : new String(),
        user_surname : new String(),
        band_id : new Number(),
        band_name : new String(),
        band_description : new String(),
        band_address : new String(),
        band_type : new String()
    };

    db.query("SELECT users.user_id, users.name as user_name, users.surename as user_surname, bands.band_id, bands.band_name, bands.description as band_description, addresses.address as band_address, citys.name as city_name, countrys.name as country_name, types_of_bands.type as band_type FROM users JOIN bands ON (users.band_id = bands.band_id) JOIN addresses ON (addresses.address_id = bands.address_id) JOIN types_of_bands ON (types_of_bands.type_of_band_id = bands.type_of_band_id) JOIN citys ON (citys.city_id = addresses.city_id) JOIN countrys ON (countrys.country_id = addresses.country_id) WHERE users.user_id = ? LIMIT 1;", req.body.user_id, (err, userDataDB) => {
        if (err) res.send("Prislo je do napake: " + err);
        else {
            userData.user_name = userDataDB[0].user_name;
            userData.user_surname = userDataDB[0].user_surname;
            userData.band_id = userDataDB[0].band_id;
            userData.band_name = userDataDB[0].band_name;
            userData.band_description = userDataDB[0].band_description;
            userData.band_address = userDataDB[0].band_address + ", " + userDataDB[0].city_name + ", " + userDataDB[0].country_name;
            userData.band_type = userDataDB[0].band_type;

            res.send(userData);
        }
    });
});

router.post("/getBandSongsID/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getBandSongs/:band_id z ip naslova: " + ip);

    var band_id = req.params.band_id;

    db.query("SELECT band_song_id FROM band_has_songs WHERE band_id = ?;", band_id, (err, bandSongsIDS) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongsIDS);
        }
    });
});

router.post("/getSongsInfo/:band_song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getSongsInfo/:band_song_id z ip naslova: " + ip);

    var bandSongID = req.params.band_song_id;

    db.query("SELECT songs.song_id, songs.title, songs.lyrics, songs.order_of_parts, band_songs.order_number, band_songs.tunning FROM band_songs JOIN songs ON band_songs.song_id = songs.song_id WHERE band_songs.band_song_id = ?;", bandSongID, (err, bandSongInfo) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(bandSongInfo[0]);
        }
    });
});

router.post("/getSongTypeID/:song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getSongTypeID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT type_of_song_id FROM types_of_songs_has_songs WHERE song_id = ?", songID, (err, typesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typesIDs);
        }
    });
});

router.post("/getSongType/:type_of_song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getSongType/:type_of_song_id z ip naslova: " + ip);

    var typeOfSongID = req.params.type_of_song_id;

    db.query("SELECT type FROM types_of_songs WHERE type_of_song_id = ?", typeOfSongID, (err, typeOfSong) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(typeOfSong[0]);
        }
    });
});

router.post("/getSongArtistID/:song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getSongArtistID/:song_id z ip naslova: " + ip);

    var songID = req.params.song_id;

    db.query("SELECT artist_id FROM artists_has_songs WHERE song_id = ?", songID, (err, artistIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistIDs);
        }
    });
});

router.post("/getArtistData/:artist_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getArtistData/:artist_id z ip naslova: " + ip);

    var artistID = req.params.artist_id;

    db.query("SELECT name FROM artists WHERE artist_id = ?", artistID, (err, artistData) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(artistData[0]);
        }
    });
});

router.post("/getBandPerfomances/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getBandPerfomances/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT performance_id, date, time, description, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM perfomances JOIN addresses ON (perfomances.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE perfomances.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.post("/getBandPractices/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getBandPractices/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;

    db.query("SELECT practice_id, date, time, addresses.address, citys.name AS city_name, countrys.name AS country_name FROM practices JOIN addresses ON (practices.address_id = addresses.address_id) JOIN citys ON (addresses.city_id = citys.city_id) JOIN countrys ON (addresses.country_id = countrys.country_id) WHERE practices.band_id = ?;", bandID, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(value);
        }
    });
});

router.post("/getPracticeSongsID/:practice_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getPracticeSongsID/:practice_id z ip naslova: " + ip);

    var practiceID = req.params.practice_id;

    db.query("SELECT band_song_id FROM practices_songs WHERE practice_id = ?;", practiceID, (err, practicesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(practicesIDs);
        }
    });
});

router.post("/getAddressInfo/:address_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllSongTypes z ip naslova: " + ip);

    var addressID = req.params.address_id;

    db.query("SELECT city_id, country_id, addresses.address, city_id, country_id, citys.name as city_name, countrys.name as country_name FROM addresses JOIN citys ON addresses.city_id = citys.city_id JOIN countrys ON addresses.country_id = countrys.country_id WHERE addresses.address_id = ?;", addressID, (err, addressData) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send( { address_name : addressData[0].address, city_name : addressData[0].city_name, country_name : addressData[0].country_name } );
        }
    });
});

router.post("/getAllCitys", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllCitys z ip naslova: " + ip);

    db.query("SELECT * FROM citys;", (err, allCityIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allCityIDs);
        }
    });
});

router.post("/getAllCountrys", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllCountrys z ip naslova: " + ip);

    db.query("SELECT * FROM countrys;", (err, allCountrysIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allCountrysIDs);
        }
    });
});

router.post("/getAllBandTypes", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllBandTypes z ip naslova: " + ip);

    db.query("SELECT * FROM types_of_bands;", (err, allBandTypesIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allBandTypesIDs);
        }
    });
});

router.post("/getAllBandIDs", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllBandIDs z ip naslova: " + ip);

    db.query("SELECT band_id, band_name FROM bands;", (err, allBandIDs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allBandIDs);
        }
    });
});

router.post("/getSpecBandID/:band_name", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getSpecBandID/:band_name z ip naslova: " + ip);

    var band_name = req.params.band_name;

    db.query("SELECT band_id FROM bands WHERE band_name = ?;", band_name, (err, bandID) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (bandID[0] == null) {
                res.send( { band_id: -1 } );
            } else {
                res.send(bandID[0]);
            }
        }
    });
});

router.post("/getAllSongExisting", userMiddleware(), (req,res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllSong z ip naslova: " + ip);

    db.query("SELECT song_id, title, lyrics, order_of_parts, original FROM songs WHERE accepted = 1;", (err, allSongs) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allSongs);
        }
    });
});

router.post("/getAllArtists", userMiddleware(), (req,res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllArtists z ip naslova: " + ip);

    db.query("SELECT * FROM artists;", (err, allArtists) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allArtists);
        }
    });
});

router.post("/getAllSongTypes", userMiddleware(), (req,res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /web/getAllSongTypes z ip naslova: " + ip);

    db.query("SELECT * FROM types_of_songs;", (err, allTypes) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send(allTypes);
        }
    });
});

// Vstavlanje podatkov

router.post("/addNewBand", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewBand z ip naslova: " + ip);

    var band_name = req.body.band_name;
    var band_desc = req.body.band_description;
    var band_type_id = req.body.band_type_id;
    var band_address = req.body.band_address;
    var city_id = req.body.city_id;
    var country_id = req.body.country_id;
    
    db.query("INSERT INTO addresses VALUES (null, ?, ?, ?);", [city_id, country_id, band_address], (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var addressID = value.insertId;
            db.query("SELECT EXISTS(SELECT * FROM bands WHERE band_name = ?) AS obstaja;", band_name, (err, value) => {
                if (err) {
                    console.log("Prislo je do napake: " + err);
                    res.send("Prislo je do napake: " + err);
                } else {
                    if (value[0].obstaja == 0) {
                        db.query("INSERT INTO bands VALUES (null, ?, ?, ?, 0, ?);", [band_type_id, addressID, band_name, band_desc], (err, newBandID) => {
                            if (err) {
                                console.log("Prislo je do napake: " + err);
                                res.send("Napak pri vstavlanju: " + err);
                            } else {
                                var backData = {
                                    band_id : newBandID.insertId,
                                    band_address_id : addressID
                                };
                                res.send(backData);
                            }
                        });
                    } else {
                        res.send( { band_id : -1, band_address_id: -1 } );
                    }
                }
            });
        }
    });
});

router.post("/addUserToBand/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addUserToBand/:band_id z ip naslova: " + ip);

    var bandID = req.params.band_id;
    var userID = req.body.user_id;

    db.query("UPDATE users SET band_id = ? WHERE user_id = ?;", [bandID, userID], (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send("OK");
        }
    });
});

router.post("/addNewSong", userMiddleware(), (req,res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewSong z ip naslova: " + ip);

    var song_title = req.body.song_title;
    var song_lyrics = req.body.song_lyrics;
    var original = req.body.original;
    var order_of_parts = req.body.order_of_parts;
    var artist_id_array = req.body.artist_id_array;
    var type_of_song_id_array = req.body.song_type_id_array;

    if (original == 1) {
        db.query("SELECT EXISTS(SELECT * FROM songs WHERE title = ?) AS obstaja;", song_title, (err, value) => {
            if (err) {
                console.log("Prislo je do napake: " + err);
                res.send("Prislo je do napake: " + err);
            } else {
                if (value[0].obstaja == 0) {
                    db.query("INSERT INTO songs VALUES (null, ?, ?, ?, ?, 0);", [song_title, song_lyrics, original, order_of_parts], (err, newSongID) => {
                        if (err) {
                            console.log("Prislo je do napake: " + err);
                            res.send("Prislo je do napake: " + err);
                        } else {
                            var songID = newSongID.insertId;

                            console.log(artist_id_array);
                        
                            /*artist_id_array.forEach(artistID => {
                                db.query("INSERT INTO artists_has_songs VALUES (null, ?, ?);", [artistID, songID], (err) => {
                                    if (err) {
                                        console.log("Prislo je do napake: " + err);
                                    } else {
                                        console.log("OK");
                                    }
                                });
                            });
                            
                            type_of_song_id_array.forEach(typeID => {
                                db.query("INSERT INTO types_of_songs_has_songs VALUES (null, ?, ?);", [typeID, songID], (err) => {
                                    if (err) {
                                        console.log("Prislo je do napake: " + err);
                                    } else {
                                        console.log("OK");
                                    }
                                });
                            });*/
            
                            res.send( { song_id : songID } );
                        }
                    });
                } else {
                    res.send( { song_id : -1 } );
                }
            }
        });
    } else {
        db.query("INSERT INTO songs VALUES (null, ?, ?, ?, ?, 0);", [song_title, song_lyrics, original, order_of_parts], (err, newSongID) => {
            if (err) {
                console.log("Prislo je do napake: " + err);
                res.send("Prislo je do napake: " + err);
            } else {
                var songID = newSongID.insertId;

                console.log(artist_id_array);
            
                /*artist_id_array.forEach(artistID => {
                    db.query("INSERT INTO artists_has_songs VALUES (null, ?, ?);", [artistID, songID], (err) => {
                        if (err) {
                            console.log("Prislo je do napake: " + err);
                        } else {
                            console.log("OK");
                        }
                    });
                });
                
                type_of_song_id_array.forEach(typeID => {
                    db.query("INSERT INTO types_of_songs_has_songs VALUES (null, ?, ?);", [typeID, songID], (err) => {
                        if (err) {
                            console.log("Prislo je do napake: " + err);
                        } else {
                            console.log("OK");
                        }
                    });
                });*/

                res.send( { song_id : songID } );
            }
        });
    }
});

router.post("/addSongToBand/:band_id/:song_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addSongToBand/:band_id/:song_id z ip naslova: " + ip);

    var bandID = req.params.band_id;
    var songID = req.params.song_id;
    var order_number = req.body.order_number;
    var tunning = req.body.tunning;

    db.query("INSERT INTO band_songs VALUES (null, ?, ?, ?);", [songID, order_number, tunning], (err, newBandSongID) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var bandSongID = newBandSongID.insertId;

            db.query("INSERT INTO band_has_songs VELUES (null, ?, ?);", [bandSongID, bandID], (err) => {
                if (err) {
                    console.log("Prislo je do napake: " + err);
                } else {
                    console.log("OK");
                }
            });

            res.send( { band_song_id : bandSongID } );
        }
    });
});

router.post("/addNewPerformanceForBand/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewPerformanceForBand/:band_id z ip naslova: " + ip);

    var band_id = req.params.band_id;
    var perf_date = req.body.date;
    var perf_time = req.body.time;
    var perf_desc = req.body.description;
    var perf_address = req.body.address;
    var city_id = req.body.city_id;
    var country_id = req.body.country_id;

    db.query("INSERT INTO addresses VALUES (null, ?, ?, ?);", [city_id, country_id, perf_address], (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var addressID = value.insertId;
            
            db.query("INSERT INTO performances VALUES (null, ?, ?, ?, ?, ?);", [band_id, addressID, perf_date, perf_time, perf_desc], (err, newPerfID) => {
                if (err) {
                    console.log("Prislo je do napake: " + err);
                    res.send("Prislo je do napake: " + err);
                } else {
                    res.send( { perfomance_id : newPerfID.insertId, perfomance_address_id : addressID } );
                }
            });
        }
    });
});

router.post("/addNewPracticeForBand/:band_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewPracticeForBand/:band_id z ip naslova: " + ip);

    var band_id = req.params.band_id;
    var prac_date = req.body.date;
    var prac_time = req.body.time;
    var prac_address = req.body.address;
    var city_id = req.body.city_id;
    var country_id = req.body.country_id;

    db.query("INSERT INTO addresses VALUES (null, ?, ?, ?);", [city_id, country_id, prac_address], (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            var addressID = value.insertId;
            
            db.query("INSERT INTO practices VALUES (null, ?, ?, ?, ?);", [band_id, addressID, prac_date, prac_time], (err, newPracID) => {
                if (err) {
                    console.log("Prislo je do napake: " + err);
                    res.send("Prislo je do napake: " + err);
                } else {
                    res.send( { practice_id : newPracID.insertId, practice_address_id : addressID } );
                }
            });
        }
    });
});

router.post("/addBandSongToPractice/:band_song_id/:practice_id", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addBandSongToPractice/:band_song_id/:practice_id z ip naslova: " + ip);

    var bandSongID = req.params.band_song_id;
    var pracID = req.params.practice_id;

    db.query("INSERT INTO practices_songs VALUES (null, ?, ?);", [bandSongID, pracID], (err) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            res.send("OK");
        }
    });
});

// Dodajanej osnovnih stvari

router.post("/addNewArtist", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewArtist z ip naslova: " + ip);

    var artist_name = req.body.artist_name;

    db.query("SELECT EXISTS(SELECT * FROM artists WHERE name = ?) AS obstaja;", artist_name, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO artists VALUES (null, ?);", artist_name, (err, newArtistID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Prislo je do napake: " + err);
                    } else {
                        res.send( { artist_id : newArtistID.insertId } );
                    }
                })
            } else {
                res.send( { artist_id : -1 } );
            }
        }
    });
});

router.post("/addNewSongType", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewSongType z ip naslova: " + ip);

    var song_type = req.body.song_type;

    db.query("SELECT EXISTS(SELECT * FROM types_of_songs WHERE type = ?) AS obstaja;", song_type, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO types_of_songs VALUES (null, ?);", song_type, (err, newTypeID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Prislo je do napake: " + err);
                    } else {
                        res.send( { type_of_song_id : newTypeID.insertId } );
                    }
                })
            } else {
                res.send( { type_of_song_id : -1 } );
            }
        }
    });
});

router.post("/addNewBandType", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewBandType z ip naslova: " + ip);

    var bandType = req.body.band_type;

    db.query("SELECT EXISTS(SELECT * FROM types_of_bands WHERE type = ?) AS obstaja;", bandType, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO types_of_bands VALUES (null, ?);", bandType, (err, newTypeID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Prislo je do napake: " + err);
                    } else {
                        res.send( { type_of_band_id : newTypeID.insertId } );
                    }
                })
            } else {
                res.send( { type_of_band_id : -1 } );
            }
        }
    });
});

router.post("/addNewCity", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewCity z ip naslova: " + ip);

    var cityName = req.body.city_name;

    db.query("SELECT EXISTS(SELECT * FROM citys WHERE name = ?) AS obstaja;", cityName, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO citys VALUES (null, ?);", cityName, (err, newCityID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Prislo je do napake: " + err);
                    } else {
                        res.send( { city_id : newCityID.insertId } );
                    }
                })
            } else {
                res.send( { city_id : -1 } );
            }
        }
    });
});

router.post("/addNewCountry", userMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /web/addNewCountry z ip naslova: " + ip);

    var countryName = req.body.country_name;

    db.query("SELECT EXISTS(SELECT * FROM countrys WHERE name = ?) AS obstaja;", countryName, (err, value) => {
        if (err) {
            console.log("Prislo je do napake: " + err);
            res.send("Prislo je do napake: " + err);
        } else {
            if (value[0].obstaja == 0) {
                db.query("INSERT INTO countrys VALUES (null, ?);", countryName, (err, newCountryID) => {
                    if (err) {
                        console.log("Prislo je do napake: " + err);
                        res.send("Prislo je do napake: " + err);
                    } else {
                        res.send( { country_id : newCountryID.insertId } );
                    }
                })
            } else {
                res.send( { country_id : -1 } );
            }
        }
    });
});

module.exports = router;

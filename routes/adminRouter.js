const express = require("express");
const path = require("path");
const crypto = require("crypto");

var db = require("../controllers/databaseController");

var adminMiddleware = require("../middleware/adminMiddleware");

var router = express.Router();

const passwordForCrypt = "9sd921iufds8";

function encrypt(text){
    var cipher = crypto.createCipher('aes-256-cbc', passwordForCrypt);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}

router.get("/", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /admin z ip naslova: " + ip);

    res.redirect("/admin/login");
});

router.get("/docks", (req, res) => {
    res.render(path.resolve("views/docks"));
});

router.get("/login", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /admin/login z ip naslova: " + ip);
    
    res.render(path.resolve("views/adminLogin"));
});

router.post("/login", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /admin/login z ip naslova: " + ip);

    console.log("Admin prijava z uporabniskim imenom: " + req.body.username);
    console.log("Admin prijava z uporabniskim geslom: " + req.body.password);

    db.query("SELECT user_id, status FROM maintenance WHERE username = ? AND password = ? LIMIT 1", [req.body.username, req.body.password], (err, value) => {
        if (err) {
            console.log("Pri prijavi je prislo do napake: " + err);
            res.redirect("/admin/wrongData");
        } else if (value[0] == null) {
            console.log("Napacni podatki za admina");
            res.redirect("/admin/wrongData");
        } else {
            res.cookie("StatusOfUser", encrypt(value[0].status), {maxAge: 3600000});
            res.redirect("/admin/dashboard/" + value[0].user_id);
        }
    });
});

router.get("/logout", adminMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /admin/logout z ip naslova: " + ip);

    res.clearCookie("StatusOfUser");
    res.redirect("/admin/login");
});

router.get("/dashboard/:id", adminMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /admin/dasboard z ip naslova: " + ip + ", z admin id:" + req.params.id);

    db.query("SELECT username FROM maintenance WHERE user_id = ? LIMIT 1", req.params.id, (err, value) => {
        if (err) {
            console.log("Pri prijavi je prislo do napake: " + err);
            res.send("Neke je slo narobe...");
        } else {
            db.query("SELECT * FROM songs WHERE accepted = 0", (err, songs) => {
                if (err) console.log("Prislo je do napake: " + err);
                else {
                    res.render(path.resolve("views/adminDashboard"), { username : value[0].username, songs : songs, user_id : req.params.id});     
                }
            });
        }
    });
});

router.get("/wrongData", (req, res) => {
    res.render(path.resolve("views/wrongPassword"));
});

router.get("/addTestData", adminMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /admin/addTestData z ip naslova: " + ip);

    res.render(path.resolve("views/addTestData"));
});

router.post("/accSong/:song_id/:user_id", adminMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /accSong/{song_id} z ip naslova: " + ip);
    
    db.query("UPDATE songs SET accepted = 1 WHERE song_id = ?", req.params.song_id, (err) => {
        if (err) console.log("Prislo je do napake: " + err);
        else res.redirect("/admin/dashboard/" + req.params.user_id);
    })
});

router.post("/delSong/:song_id/:user_id", adminMiddleware(), (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("POST => /accSong/{song_id} z ip naslova: " + ip);
    
    db.query("DELETE FROM songs WHERE song_id = ?", req.params.song_id, (err) => {
        if (err) console.log("Prislo je do napake: " + err);
        else res.redirect("/admin/dashboard/" + req.params.user_id);
    })
});

module.exports = router;

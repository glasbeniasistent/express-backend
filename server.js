const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const https = require("https");
const fs = require("fs");

const port = 2800;
const app = express();

var adminRouter = require("./routes/adminRouter.js");

var mobileRouter = require("./routes/mobileRouter.js");

var webRouter = require("./routes/webRouter.js");

var desktopRouter = require("./routes/desktopRouter.js");

app.set("view engine", "ejs");

app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: false }));

app.use("/admin", adminRouter);

app.use("/mobile", mobileRouter);

app.use("/web", webRouter);

app.use("/desktop", desktopRouter);

app.get("/", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => / z ip naslova: " + ip);

    res.sendFile(__dirname + "/views/home.html");
});

app.get("/docks", (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("GET => /docks z ip naslova: " + ip);

    res.sendFile(__dirname + "/views/docks.html");
});

/*https.createServer({
    key : fs.readFileSync("server.key"),
    cert: fs.readFileSync("server.cert")
}, app).listen(port, (err) => {
    if (err) console.log(err);
    else console.log("Streznik se je uspesno zagnal na portu: " + port);
    });
*/

app.listen(port, (err) => {
    if (err) console.log(err);
    else console.log("Streznik se je uspesno zagnal na portu: " + port);
});

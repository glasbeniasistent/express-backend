const fs = require("fs");
const mysql = require("mysql");

var dataJSON = fs.readFileSync(__dirname + "/../config.json");
var objJSON = JSON.parse(dataJSON);

console.log("---Izpis configa---");
console.log(objJSON);

var connection = mysql.createConnection({
    host : "localhost",
    user : objJSON.usernameDB,
    password : objJSON.passwordDB,
    database : objJSON.nameDB
});

connection.connect((err) => {
    if (err) console.log("---Povezave z bazo ni bilo mogoče vzpostaviti---");
    else console.log("---Povezava z bazo je bila vzpostavljena---");
});

module.exports = connection;
const express = require("express");
const path = require("path");
const crypto = require("crypto");

const passwordForCrypt = "9sd921iufds8";

function decrypt(text){
    var decipher = crypto.createDecipher('aes-256-cbc', passwordForCrypt);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

module.exports = function() {
    return function (req, res, next) {
        if (req.cookies.StatusOfUser != null) {
            var decryptedStatus = decrypt(req.cookies.StatusOfUser);
            if (decryptedStatus == "admin") next(); 
            else res.redirect("/admin/login"); 
        } else {
            res.redirect("/admin/login");
        }
    }
}
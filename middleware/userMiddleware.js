const express = require("express");
const path = require("path");
const crypto = require("crypto");

const passwordForCrypt = "9sd921iufds8";

var db = require("../controllers/databaseController.js");

function decrypt(text){
    var decipher = crypto.createDecipher('aes-256-cbc', passwordForCrypt);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

module.exports = function() {
    return function(req, res, next) {
        var user_id = req.body.user_id;
        var sec_token = req.body.sec_token;

        if (user_id == null || sec_token == null) res.send("Napaka, neveljavni podatki");
        else {
            db.query("SELECT sec_token FROM users WHERE user_id = ?;", user_id, (err, secToken) => {
                if (sec_token == secToken[0].sec_token) next();
                else res.send("Varnostna žetona se ne ujemata");
            });
        }
    }
}